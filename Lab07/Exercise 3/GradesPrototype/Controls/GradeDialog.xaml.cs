﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GradesPrototype.Services;
using Grades.DataModel;
using System.Text.RegularExpressions;

namespace GradesPrototype.Controls
{
    /// <summary>
    /// Interaction logic for GradeDialog.xaml
    /// </summary>
    public partial class GradeDialog : Window
    {
        public GradeDialog()
        {
            InitializeComponent();
        }

        private void GradeDialog_Loaded(object sender, RoutedEventArgs e)
        {
            // Display the list of available subjects in the subject ListBox
            foreach (Subject subj in SessionContext.DBContext.Subjects)
            {
                subject.Items.Add(subj.Name);
            }

            // Set default values for the assessment date and subject
            assessmentDate.SelectedDate = DateTime.Now;
            subject.SelectedValue = subject.Items[0];
        }

        // If the user clicks OK to save the Grade details, validate the information that the user has provided
        private void ok_Click(object sender, RoutedEventArgs e)
        {
            // Create a Grade object and use it to trap and report any data validation exceptions that are thrown
            try
            {
                // TODO: Exercise 3: Task 2a: Create a Grade object.
                Grade grade = new Grade()
                {
                    AssessmentDate = DateTime.Parse(assessmentDate.Text),
                    Assessment = assessmentGrade.Text
                };

                
                // TODO: Exercise 3: Task 2b: Call the ValidateAssessmentDate method.
                ValidateAssessmentDate(grade.AssessmentDate);

                // TODO: Exercise 3: Task 2c: Call the ValidateAssessmentGrade method.
                ValidateAssessmentGrade(grade.Assessment);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error creating assessment", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Indicate that the data is valid
            this.DialogResult = true;
        }

        public void ValidateAssessmentDate(DateTime assessmentDate)
        {
            if (assessmentDate > DateTime.Now)
            {
                throw new ArgumentOutOfRangeException("Assessment Date", "Assessment date must be now or before now");
            }
        }

        public void ValidateAssessmentGrade(string assessment)
        {
            Match matchGrade = Regex.Match(assessment, @"^[A-E][+-]?$");

            if (!matchGrade.Success)
            {
                throw new ArgumentOutOfRangeException("Assessment", "Assessment must be A+ to E-");
            }
        }
    }
}
